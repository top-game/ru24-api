﻿using RU24ServerLib;

namespace Simple.RU24.ServerLibary
{
    /// <summary>
    /// Простой сервер
    /// Для примера
    /// </summary>
    public class SimpleServer : IServer
    {
        /// <summary>
        /// Запрос на загрузку мода или же на вход в игру
        /// </summary>
        public void LoadRequest()
        {
            //Простой код, вызовите любое исключение для того, чтобы отвергнуть
            //запрос и не переходить к загрузке или входу в игру
        }

        /// <summary>
        /// Загрузка мода, останавливает поток
        /// </summary>
        public void Load()
        {
            //Загрузка мода, на самом деле лучше использовать метод, который не
            //останавливает поток, здесь можно поместить ваш Thread
        }

        public void AsyncLoad()
        {
            //Асинхронная загрузка мода, самый оптимальный способ сохранять
            //поток интерфейса постоянно открым, при этом можно вызывать
            //изменять _localLWS переменную, тогда будет менятся интерфейс
        }

        /// <summary>
        /// Запрос авторизации на сайте/сервере (просходит ТОЛЬКО перед запуском игры!)
        /// </summary>
        public void AuthRequest()
        {
            //Запрос входа, полезно если ваш сервер проверяет игроков, которые
            //вошли на сервер через лаунчер или нет. Здесь можно делать всё, от
            //отправки POST запросов, до коннекта с базой данных.
        }

        /// <summary>
        /// Игра запущена, выполняется в одном потоке с лаунчером
        /// </summary>
        public void GameRunned()
        {
            //Не рекомендуется, т.к. может нарушать производительность, но если
            //вы хотите менять интерфейс лаунчера, можно использовать этот метод
        }

        /// <summary>
        /// Игра запущена, выполняется в другом потоке
        /// </summary>
        public void AsyncGameRunned()
        {
            //Здесь можно запустить античит систему, проверять процесс игры,
            //делать всё что захотите
        }

        /// <summary>
        /// Загрузка DLL модуля
        /// </summary>
        public void Initialized()
        {
            //Если вызвать любое исключение загрузка будет прекращена
        }

        private LoadWindowsSettings _localLWS = new LoadWindowsSettings
        {
            IsIndeterminate = true,
            Progress = 50
        };

        /// <summary>
        /// Предоставляет сведения для формы загрузки
        /// </summary>
        public LoadWindowsSettings lws
        {
            get
            {
                return _localLWS;
            }
        }

        /// <summary>
        /// Информация о сервере
        /// </summary>
        public ServerInfo ServerInfo
        {
            get
            {
                return new ServerInfo
                {
                    name = "Simple server",
                    author = "Mr. Nobody",
                    version = "some version",
                    players_max = 69
                };
            }
        }
    }
}